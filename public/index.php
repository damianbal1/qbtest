<?php

include __DIR__ . '/../vendor/autoload.php';

$query = [
    'filters' => [
        [
            'type' => 'match',
            'field' => 'username',
            'value' => 'damian'
        ],
        [
            'type' => 'range',
            'field' => 'age',
            'value' => ['min' => 17]
        ],
        [
            'type' => 'parameter',
            'field' => 'friends_nested',
            'path' => 'friends_nested',
            'value' => [
                [
                    'type' => 'match',
                    'field' => 'name',
                    'value' => 'ewa',
                    'path' => 'friends_nested'
                ],
                [
                    'type' => 'range',
                    'field' => 'age',
                    'value' => ['min' => 17],
                    'path' => 'friends_nested'
                ],
            ],
            'path' => 'friends_nested',
        ]
    ]
];

$builder = new \DamianBal\QBTest\Builder();
$builder->addQueryBuilder('match', \DamianBal\QBTest\QueryBuilder\MatchQueryBuilder::class);
$builder->addQueryBuilder('range', \DamianBal\QBTest\QueryBuilder\RangeQueryBuilder::class);
$builder->addQueryBuilder('parameter', \DamianBal\QBTest\QueryBuilder\ParameterQueryBuilder::class);

$query = $builder->buildQuery($query);

/*
echo '<pre>' . var_export($query, true) . '</pre>';
*/
header('Content-Type: application/json');
echo json_encode($query);