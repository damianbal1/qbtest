<?php

namespace DamianBal\QBTest;

class Builder
{
    protected $queryBuilders = [];

    public function addQueryBuilder(string $name, string $queryBuilderClass)
    {
        $this->queryBuilders[$name] = new $queryBuilderClass;
    }

    public function getQueryBuilder($type) {
        return isset($this->queryBuilders[$type]) ? $this->queryBuilders[$type] : null;
    }

    public function buildQuery($input)
    {
        $filters = $input['filters'];
        $output = [];

        foreach ($filters as $filter) {
            $queryBuilder = $this->getQueryBuilder($filter['type']);

            if (!$queryBuilder) {
                echo "There is no query builder for that type: "  . $filter['type'];
                die;
            }

            $output['query'][] = $queryBuilder->build($filter);
        }

        return $output;
    }
}