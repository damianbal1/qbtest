<?php

namespace DamianBal\QBTest\QueryBuilder;


// https://www.elastic.co/guide/en/elasticsearch/reference/6.4/query-dsl-nested-query.html
// https://www.elastic.co/guide/en/elasticsearch/reference/current/nested.html
class ParameterQueryBuilder extends QueryBuilder
{
    public function __construct()
    {
        $this->queryBuildersMap = [
            'match' => MatchQueryBuilder::class,
            'range' => RangeQueryBuilder::class,
            'parameter' => ParameterQueryBuilder::class
        ];
    }

    protected $queryBuildersMap = [];

    protected function getQueryBuilderForType($type)
    {
        return new $this->queryBuildersMap[$type];
    }

    public function build($filter)
    {
        $filters = $filter['value'];

        $output = [
            'nested' => [
                'path' => $filter['path'],
                'query' => [
                    'bool' => [
                        'must' => [

                        ]
                    ]
                ]
            ]
        ];

        foreach ($filters as $filter_) {
            $type = $filter_['type'];
            unset($filter_['path']);
            $queryBuilder = $this->getQueryBuilderForType($type);

            if ($queryBuilder) {
                $output['nested']['query']['bool']['must'][] = $queryBuilder->build($filter_);
            }
        }

        return $output;
    }
}