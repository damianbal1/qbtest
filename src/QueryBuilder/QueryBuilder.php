<?php
/**
 * Created by PhpStorm.
 * User: programowanie
 * Date: 2019-02-16
 * Time: 11:44
 */

namespace DamianBal\QBTest\QueryBuilder;


abstract class QueryBuilder
{
    public abstract function build($parameter);
}