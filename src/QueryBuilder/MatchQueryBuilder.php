<?php

namespace DamianBal\QBTest\QueryBuilder;


// https://www.elastic.co/guide/en/elasticsearch/reference/6.4/query-dsl-match-query.html
class MatchQueryBuilder extends QueryBuilder
{
    public function build($filter)
    {
        $result = [];

        $result['match'] = [
            $filter['field'] => $filter['value']
        ];

        if (isset($filter['path'])) {
            $result = [
                'nested' => $result
            ];
        }

        return $result;
    }
}