<?php

namespace DamianBal\QBTest\QueryBuilder;


// https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-range-query.html
class RangeQueryBuilder extends QueryBuilder
{
    public function build($filter)
    {
        $result = [];

        $result = [
            'range' => [
                $filter['field'] => [
                    'gte' => $filter['value']['min'] ?? null,
                    'lte' => $filter['value']['max'] ?? null,
                ]
            ]
        ];

        if (is_null($result['range'][$filter['field']]['gte'])) {
            unset($result['range'][$filter['field']]['gte']);
        }

        if (is_null($result['range'][$filter['field']]['lte'])) {
            unset($result['range'][$filter['field']]['lte']);
        }

        if (!empty($filter['path'])) {
            $result = ['nested' => [$result, 'path' => $filter['path']]];
        }

        return $result;
    }
}